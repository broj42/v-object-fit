module.exports = {
  inserted: function(el, binding) {
    binding.def.init(el, binding)
  },
  update: function(el, binding){
    binding.def.init(el, binding)
  },
  init: function(el, binding){
    let ua = window.navigator.userAgent;
    if (ua.indexOf('MSIE ') > 0 || ua.indexOf('Edge/') > 0 || ua.indexOf('Trident/') > 0) {
      let backgroundSize = binding.value !== undefined ? binding.value : 'cover'
      
      if(backgroundSize === 'fill') backgroundSize = '100% 100%';

      el.style.backgroundSize = backgroundSize;
      el.style.backgroundRepeat = 'no-repeat';

      if(el.src) el.style.backgroundImage = 'url('+el.src+')';
      else if(el.dataset.src) el.style.backgroundImage = 'url('+el.dataset.src+')';
      el.style.backgroundPosition = 'center center';
      el.dataset.src = el.src;
      el.removeAttribute('src');
    }
  }
};