# v-object-fit directive
## Usage
| npm install v-object-fit | yarn add v-object-fit |
|--|--|

```javascript
import Vue from 'vue';
import vObjectFit from 'v-object-fit';
Vue.directive('object-fit', vObjectFit);
```
```html
<img src="https://picsum.photos/536/354" alt="Image Alt" title="Image Title" v-object-fit>
```
## Options
By default object-fit will be **cover**, if you want to use other options, you can do it like this:
```html
<img src="https://picsum.photos/536/354" alt="..." title="..." v-object-fit="contain">
```